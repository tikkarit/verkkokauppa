<h3><?= $otsikko?></h3>
<div>
  <?= \Config\Services::validation()->listErrors();?>
</div>
<form action="/manager/tallennatr" method="post">
  <input type="hidden" name="id" value="<?= $id?>">
  <div>
    <label>Nimi</label>
    <input name="trnimi" maxlength="50" value="<?= $trnimi?>"/>
  </div>
  <button>Tallenna</button>
</form>