drop DATABASE IF EXISTS  verkkokauppa;

create database verkkokauppa;

use verkkokauppa;

create table tuoteryhma(
    id INT AUTO_INCREMENT PRIMARY KEY,
    trnimi varchar(100) NOT NULL
);

create table kategoria(
    id INT AUTO_INCREMENT PRIMARY KEY,
    kategoria_nimi varchar(100) NOT NULL,
    tuoteryhma_id INT NOT NULL,
    index (tuoteryhma_id ),
    foreign KEY (tuoteryhma_id) REFERENCES tuoteryhma(id)
    ON delete RESTRICT 
);

CREATE TABLE tuote(
    id INT AUTO_INCREMENT PRIMARY KEY,
    tuotenimi varchar(100) NOT NULL,
    hinta decimal(5,2) NOT NULL,
    kuvaus varchar(200) NOT NULL,
    kuva varchar(50) NOT NULL,
    kategoria_id INT NOT NULL,
    maara INT NOT NULL DEFAULT 0,
    index (kategoria_id),
    foreign KEY (kategoria_id) REFERENCES kategoria(id)
    ON delete RESTRICT 
);

CREATE TABLE asiakas(
    id INT PRIMARY key AUTO_INCREMENT,
    kayttajatunnus VARCHAR (100) NOT NULL UNIQUE ,
    salasana VARCHAR (100) NOT NULL ,
    admin varchar(50) NOT NULL ,
    etunimi VARCHAR (100) NOT NULL ,
    sukunimi VARCHAR (100) NOT NULL ,
    osoite VARCHAR (100) NOT NULL ,
    email NVARCHAR(255) NOT NULL,
    postinro VARCHAR(6) NOT NULL,
    postitmp VARCHAR (100) NOT NULL ,
    puhelinro VARCHAR(20) NOT NULL
);

CREATE TABLE tilaus (
    id INT AUTO_INCREMENT PRIMARY KEY,
    tilauspvm TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    asiakas_id INT NOT  NULL ,
    index (asiakas_id),
    FOREIGN  KEY (asiakas_id) REFERENCES asiakas(id)
    on delete RESTRICT,
    tapa varchar(1) ,
    tila varchar(1) 
);

CREATE TABLE tilausrivi (
    tilaus_id INT  NOT  NULL,
    index (tilaus_id),
    FOREIGN  KEY (tilaus_id) REFERENCES tilaus(id)
    on delete RESTRICT,
    tuote_id INT  NOT  NULL,
    index (tuote_id),
    FOREIGN  KEY (tuote_id) REFERENCES tuote(id)
    on delete RESTRICT,
    kpl varchar(100) NOT NULL
);


/**Tuotteiden järjestäminen ryhmiin ja kategorioihin tietokantaan.*/

insert into tuoteryhma (trnimi) values('Oluet');
insert into tuoteryhma (trnimi) values('Naposteltavat');
insert into tuoteryhma (trnimi) values('Tarvikkeet');

insert into kategoria (tuoteryhma_id,kategoria_nimi) values(1,'Ipat');
insert into kategoria (tuoteryhma_id,kategoria_nimi) values(1,'Lagerit');
insert into kategoria (tuoteryhma_id,kategoria_nimi) values(1,'Muut');

insert into kategoria (tuoteryhma_id,kategoria_nimi) values(2,'Naksut');
insert into kategoria (tuoteryhma_id,kategoria_nimi) values(2,'Kuivalihat');

insert into kategoria (tuoteryhma_id,kategoria_nimi) values(3,'Tuopit ja lasit');
insert into kategoria (tuoteryhma_id,kategoria_nimi) values(3,'Krääsä');

/**Tuotteiden hinta ja muita juttuja .*/
insert into tuote (tuotenimi, hinta, kuvaus, kuva,kategoria_id) 
values ('Hailuodon Altbier',8,'Hailuodon panimon saksalaisella altbier-perinteellä valmistettu pintahiivaolut. 4,3 %','altbier.jpg',3);

insert into tuote (tuotenimi, hinta, kuvaus, kuva,kategoria_id) 
values ('Lapin Panimon Amber Lager',9,'Riski-nimellä nykyään pantava Lapin Panimon Amber lager. Pohjahiivaolueksi vahva ja täyteläinen. 6,5 %','amberlager.jpg',2);

insert into tuote (tuotenimi, hinta, kuvaus, kuva,kategoria_id) 
values ('Hailuodon Festbier',8,'Hailuodon panimon luomulager, Festbier. Pitkiin ja juhlaviin hetkiin. 5,3 %','festbier.jpg',2);

insert into tuote (tuotenimi, hinta, kuvaus, kuva,kategoria_id) 
values ('Lapin Panimon Hölmö IPA',8,'Lapin Panimon Hölmö IPA, hedelmäinen ja raikas. 5,4 %','holmoipa.jpg',1);

insert into tuote (tuotenimi, hinta, kuvaus, kuva,kategoria_id) 
values ('Lapin Panimon Light pils',7.5,'Lapin Panimon Lapin Suvi Light pils. Saa sinut lausumaan Leinoa Loirin tavoin. 2,5 %','lightpils.jpg',3);

insert into tuote (tuotenimi, hinta, kuvaus, kuva,kategoria_id) 
values ('Hailuodon Pilsneri',7,'Hailuodon panimon keittomäskätty pitkä pilsneri. Suodattamaton, kuten nautintosikin ensihörpyn jälkeen!','pilsneri.jpg',3);

insert into tuote (tuotenimi, hinta, kuvaus, kuva,kategoria_id) 
values ('Suolapähkinät',3.5,'Taffelin perinteiset suolapähkinät oluen kylkeen. Tilaa useampi, yksi ei riitä, koska nämä koukuttavat!','suolapahkinat.png',4);

insert into tuote (tuotenimi, hinta, kuvaus, kuva,kategoria_id) 
values ('Pyynikin Premium Ipa',4.0,'Pyynikin Premium Ipa, varsin pehmeä ja englantilaistyylinen. 4,2 %','pyynikinipa.jpg',1);

insert into tuote (tuotenimi, hinta, kuvaus, kuva,kategoria_id) 
values ('Saimaan IPA',4.0,'Saimaa Brewing Companyn viettelevä kumppani. Viileän vaalea hipauksella sitrusta. 4,5 %','saimaaipa.jpg',1);

insert into tuote (tuotenimi, hinta, kuvaus, kuva,kategoria_id) 
values ('Tornion Original Lager',3.5,'Tornion panimon se aito ja alkuperäinen "Lappari". Turha yrittääkään väittää, etteikö sinullakin olisi ollut sitä ikävä! 5,2 %','tornionlager.jpg',2);

insert into tuote (tuotenimi, hinta, kuvaus, kuva,kategoria_id) 
values ('Tornion Lapland Red Ale (G)',3.5,'Tornion panimon karamellimaisen pehmeä gluteeniton ale. Tässä maistuu aito Lappi, vaikka onkin irlantilaistyylinen. 5,2 %','tornionredale.jpg',3);

insert into tuote (tuotenimi, hinta, kuvaus, kuva,kategoria_id) 
values ('Jerkku Original',13.50,'Parempaa kaveria oluen nauttimiseen ei voisi toivoa, kuin Kuivalihakundin maukas Original Jerkku. Jos hiilarit pelottaa, vähennä naksuista, älä oluesta!','jerkkuoriginal.png',5);

insert into tuote (tuotenimi, hinta, kuvaus, kuva,kategoria_id) 
values ('Paahdetut mantelit',2.5,'Rainbown paahdetut ja suolatut mantelit. Jos pähkinät ottaa jo päähän, tässä on mainio vaihtoehto lisäjanon saavuttamiseksi! Niin hyviä, että pussi aukesi jo kuvatessa!','mantelit.JPG',4);

insert into tuote (tuotenimi, hinta, kuvaus, kuva,kategoria_id) 
values ('Pataässä-avaaja',5,'Suorastaan välttämätön työkalu jokaiseen pokeri-iltaan, ja muutenkin, kun haluat oluesi maistuvan avaamisen jälkeen vielä astetta paremmalta!','assa.JPG',7);

insert into tuote (tuotenimi, hinta, kuvaus, kuva,kategoria_id) 
values ('Kitara-avaaja',5,'Voimme taata, että mikäli avaat oluesi tällä skeballa, voit soittaa virheettömästi Crazy Train:in soolon, jos vain olet harjoitellut tarpeeksi.','skeba.JPG',7);

insert into tuote (tuotenimi, hinta, kuvaus, kuva,kategoria_id) 
values ('Craft Beer Nuts',1.5,'Pussi on pieni, mutta sitäkin tehokkaampi! Tämä pähkinäsekoitus on kehitetty nimenomaan pienpanimo-oluen kylkeen, ja onkin laitonta nauttia ilman olutta kahdeksassa valtiossa.','pahkinat.JPG',4);

insert into tuote (tuotenimi, hinta, kuvaus, kuva,kategoria_id) 
values ('Olutlasit',15,'Tällä parivaljakolla maksimoit nautintosi olutlaadusta riippumatta. Tilavuutta riittää molemmissa, joskin Hailuodon pitkää ei kannata edes yrittää kerrallaan vain toiseen.','lasit.JPG',6);