<?php namespace App\Controllers;

use App\Models\TuoteryhmaModel;
use App\Models\KategoriaModel;
use App\Models\OstoskoriModel;
use App\Models\AsiakasModel;
use App\Models\TilausModel;
use App\Models\TilausriviModel;

class Asiakas extends BaseController
{
	private $tuoteryhmaModel=null;
	private $kategoriaModel=null;
  private $ostoskoriModel=null;
  private $asiakasModel=null;
  private $tilausModel=null;
  private $tilausriviModel=null;

	public function __construct() {
		$this->tuoteryhmaModel = new TuoteryhmaModel();
		$this->kategoriaModel = new KategoriaModel();
    $this->ostoskoriModel = new OstoskoriModel();
    $this->asiakasModel = new AsiakasModel();
    $this->tilausModel = new TilausModel();
    $this->tilausriviModel = new TilausriviModel();
		
	}
	
	public function index()
	{	
		if(!isset($_SESSION['kayttajatunnus']))
		{  			
            return redirect('login');
		}

		else if ($_SESSION['kayttajatunnus']['admin'] == 'KYLLÄ') {
            $data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
            $data['kategoriat'] = $this->kategoriaModel->haeKategoriat();
            $data['ostoskori_lkm'] = $this->ostoskoriModel->lukumaara();
            echo view('templates/header_logged_admin',$data);
            echo view('asiakas',$data);
            echo view('templates/footer');
		} else {
            $data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
            $data['kategoriat'] = $this->kategoriaModel->haeKategoriat();
            $data['ostoskori_lkm'] = $this->ostoskoriModel->lukumaara();
            echo view('templates/header_logged',$data);
            echo view('asiakas',$data);
            echo view('templates/footer');
    }
    }
    
    public function info()
	{	
        // var_dump($_SESSION['kayttajatunnus']);
        // exit;

		if(!isset($_SESSION['kayttajatunnus']))
		{  			
            return redirect('login');
		}

		else if ($_SESSION['kayttajatunnus']['admin'] == 'KYLLÄ') {
            $data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
            $data['kategoriat'] = $this->kategoriaModel->haeKategoriat();
            $data['ostoskori_lkm'] = $this->ostoskoriModel->lukumaara();
            $data['asiakas'] = $_SESSION['kayttajatunnus'];
            echo view('templates/header_logged_admin',$data);
            echo view('asiakastiedot',$data);
            echo view('templates/footer');
		} else {
            $data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
            $data['kategoriat'] = $this->kategoriaModel->haeKategoriat();
            $data['ostoskori_lkm'] = $this->ostoskoriModel->lukumaara();
            $data['asiakas'] = $_SESSION['kayttajatunnus'];
            echo view('templates/header_logged',$data);
            echo view('asiakastiedot',$data);
            echo view('templates/footer');
    }
	}


    public function tilaukset() {
      if(!isset($_SESSION['kayttajatunnus']))
      {  			
              return redirect('login');
      }
  
      else if ($_SESSION['kayttajatunnus']['admin'] == 'KYLLÄ') {
              $data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
              $data['kategoriat'] = $this->kategoriaModel->haeKategoriat();
              $data['ostoskori_lkm'] = $this->ostoskoriModel->lukumaara();
              $data['asiakas'] = $_SESSION['kayttajatunnus'];
              $data['tilaukset'] = $this->tilausModel->haeAsiakkaantilaukset($_SESSION['kayttajatunnus']['id']);
              echo view('templates/header_logged_admin',$data);
              echo view('asiakastilaukset',$data);
              echo view('templates/footer');
      } else {
              $data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
              $data['kategoriat'] = $this->kategoriaModel->haeKategoriat();
              $data['ostoskori_lkm'] = $this->ostoskoriModel->lukumaara();
              $data['asiakas'] = $_SESSION['kayttajatunnus'];
              // var_dump ($_SESSION['kayttajatunnus']);
              // exit;
              $data['tilaukset'] = $this->tilausModel->haeAsiakkaantilaukset($_SESSION['kayttajatunnus']['id']);
              echo view('templates/header_logged',$data);
              echo view('asiakastilaukset',$data);
              echo view('templates/footer');
      }
    }
    
    public function tiedot() {
        //     var_dump($_SESSION['kayttajatunnus']);
        // exit;
        if ($this->request->getMethod() === 'post') {
          if (!$this->validate([
            'kayttajatunnus' => 'required|min_length[8]|max_length[30]',
            'etunimi' => 'required|min_length[1]|max_length[100]',
            'sukunimi' => 'required|min_length[1]|max_length[100]',
            'osoite' => 'required|min_length[1]|max_length[100]',
            'postinro' => 'required|min_length[1]|max_length[100]',
            'postitmp' => 'required|min_length[1]|max_length[100]',
            'email' => 'required|min_length[1]|max_length[255]',
            'puhelinro' => 'required|min_length[1]|max_length[100]'
          ])) {  
            // Validointi ei mene läpi, palautetaan lomake näkyviin.
            $data['id'] = $this->request->getPost('id');
            $data['kayttajatunnus'] = $this->request->getPost('kayttajatunnus');
            $data['etunimi'] = $this->request->getPost('etunimi');
            $data['sukunimi'] = $this->request->getPost('sukunimi');
            $data['osoite'] = $this->request->getPost('osoite');
            $data['postinro'] = $this->request->getPost('postinro');
            $data['postitmp'] = $this->request->getPost('postitmp');
            $data['email'] = $this->request->getPost('email');
            $data['puhelinro'] = $this->request->getPost('puhelinro');
            $this->naytaLomake($data);
          }
          else { 
            $talleta['id'] = $this->request->getPost('id');
            $talleta['kayttajatunnus'] = $this->request->getPost('kayttajatunnus');
            $talleta['etunimi'] = $this->request->getPost('etunimi');
            $talleta['sukunimi'] = $this->request->getPost('sukunimi');
            $talleta['osoite'] = $this->request->getPost('osoite');
            $talleta['postinro'] = $this->request->getPost('postinro');
            $talleta['postitmp'] = $this->request->getPost('postitmp');
            $talleta['email'] = $this->request->getPost('email');
            $talleta['puhelinro'] = $this->request->getPost('puhelinro');
            // var_dump($talleta);
            // exit;
            $this->asiakasModel->save($talleta);
            $_SESSION['kayttajatunnus'] = $this->asiakasModel->haeAsiakas($talleta['id']);
            // $_SESSION['kayttajatunnus'] = $talleta;
            return redirect()->to(site_url('asiakas/info'));
          }      
        }
         else { // Näytetään lomake, koska kyseessä on get-metodi eli lomakketta ollaan vasta tuomassa näytölle.
          $data['id'] = '';
          $data['kayttajatunnus'] = '';
          $data['etunimi'] = ''; 
          $data['sukunimi'] = '';
          $data['osoite'] = '';
          $data['postinro'] = '';
          $data['postitmp'] = '';
          $data['email'] = '';
          $data['puhelinro'] = '';
         }
         $this->naytaLomake($data);
      }
    

      private function naytaLomake($data) {
        if ($_SESSION['kayttajatunnus']['admin'] == 'KYLLÄ') {
          $data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
          $data['kategoriat'] = $this->kategoriaModel->haeKategoriat();
          $data['ostoskori_lkm'] = $this->ostoskoriModel->lukumaara();
          $data['asiakas'] = $_SESSION['kayttajatunnus'];
          echo view('templates/header_logged_admin',$data);
          echo view('asiakastiedot',$data);
          echo view('templates/footer');
        } else {
          $data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
          $data['kategoriat'] = $this->kategoriaModel->haeKategoriat();
          $data['ostoskori_lkm'] = $this->ostoskoriModel->lukumaara();
          $data['asiakas'] = $_SESSION['kayttajatunnus'];
          echo view('templates/header_logged',$data);
          echo view('asiakastiedot',$data);
          echo view('templates/footer');
        }
      }
}
