<?php namespace App\Controllers;

use App\Models\TuoteryhmaModel;
use App\Models\KategoriaModel;
use App\Models\OstoskoriModel;
use App\Models\AsiakasModel;


class Home extends BaseController
{
	private $tuoteryhmaModel=null;
	private $kategoriaModel=null;
	private $ostoskoriModel=null;
	private $asiakasModel=null;

	public function __construct() {
		$this->tuoteryhmaModel = new TuoteryhmaModel();
		$this->kategoriaModel = new KategoriaModel();
		$this->ostoskoriModel = new OstoskoriModel();
		$this->asiakasModel = new AsiakasModel();
	}
	
	/**Sovelluksen etusivun näyttäminen */

	public function index()
	{	
		if(!isset($_SESSION['kayttajatunnus']))
		{
   			
		$data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
		$data['kategoriat'] = $this->kategoriaModel->haeKategoriat();
		$data['ostoskori_lkm'] = $this->ostoskoriModel->lukumaara();
		echo view('templates/header',$data);
		echo view('etusivu',$data);
		echo view('templates/footer');
		}

		else if ($_SESSION['kayttajatunnus']['admin'] == 'KYLLÄ') {
		
		$data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
		$data['kategoriat'] = $this->kategoriaModel->haeKategoriat();
		$data['ostoskori_lkm'] = $this->ostoskoriModel->lukumaara();
		echo view('templates/header_logged_admin',$data);
		echo view('etusivu',$data);
		echo view('templates/footer');
		}
		else {
		$data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
		$data['kategoriat'] = $this->kategoriaModel->haeKategoriat();
		$data['ostoskori_lkm'] = $this->ostoskoriModel->lukumaara();
		echo view('templates/header_logged',$data);
		echo view('etusivu',$data);
		echo view('templates/footer');
		}
	}

}
