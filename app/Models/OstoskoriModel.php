<?php namespace App\Models;

use CodeIgniter\Model;

/**
 * Sisältää ostoskorin käsittelyyn liittyviä metodeja.
 */
class OstoskoriModel extends Model {
  // Tilauksen yhteydessä tallennetaan tietoja myös muihin tauluhin (kuin tilaus). Nämä operaatiot
  // tehdään muiden mallien avulla. Tämä luokka käsittelee suoraan vain tilaus-taulua.
  private $tuoteModel = null;
  private $asiakasModel = null;
  private $tilausModel = null;
  private $tilausriviModel = null;

  function __construct()
  { 
    // Istunto päälle ja luodaan tarvittaessa uusi ostoskori.
    $session = \Config\Services::session();
    $session->start();
    if (!isset($_SESSION['kori'])) {
      $_SESSION['kori'] = array();
    }

    // Otetaan viittaus tietokanta-olion, jotta voidaan hallinnoida transaktoita.
    $this->db = \Config\Database::connect();
    
    // Luodaan oliot tarvittavista malleista.
    $this->tuoteModel = new TuoteModel();
    $this->asiakasModel = new AsiakasModel();
    $this->tilausModel = new TilausModel();
    $this->tilausriviModel = new TilausriviModel();
  }

  /**
   * Palauttaa ostoskorin
   * 
   * @return Array Ostoskori istuntomuuttujasta.
   */
  public function ostoskori() {
    return $_SESSION['kori'];
  }

  /**
   * Laskee tuotteiden määrän ostoskorissa.
   * 
   * @return int Tuotteiden lukumäärä ostoskorissa. 
   */
  public function lukumaara() {
    return count($_SESSION['kori']);
  }

  /**
   * Tallentaa tilauksen tietokantaan (asiakas, tilaus ja tilausrivi).
   * 
   * @param Array $asiakas Asiakkaan tiedot.
   */
  public function tilaa($asiakas) {
    // Aloitetaan transaktiot.
    $this->db->transStart(); 
    // Tallennetaan asiakas.
    // $this->asiakasModel->save($asiakas);
    // $asiakas_id = $this->insertid();
    // Tallennetaan tilaus.
    $this->tilausModel->save(['asiakas_id' => $asiakas['id']]);
    $tilaus_id = $this->insertid();
    // Tallennetaan tilausrivit.
    foreach ($_SESSION['kori'] as $tuote) {
      $this->tilausriviModel->save([
        'tilaus_id' => $tilaus_id,
        'tuote_id' => $tuote['id'],
        'kpl' => $tuote['maara']
      ]);
    }
    // Ostoskori tyhjennetään onnistuneen tilauksen jälkeen.
    $this->tyhjenna();
    // Päätetään transaktio.
    $this->db->transComplete();
  }


  /**
   * Lisää tuotteen ostoskoriin.
   * 
   * @param int $tuote_id Ostoskoriin lisättävän tuotten id.
   */
  public function lisaa($tuote_id) {
    // Haetaan tuote id:n perusteella tietokannasta tuoteModelia kautta.
		$tuote = $this->tuoteModel->hae($tuote_id);

		// Luodaan ostoskoriin lisättävä tuote (taulukko, jossa kenttinä tarvittavat tiedot.)
		$ostoskorinTuote['id'] = $tuote['id'];
		$ostoskorinTuote['tuotenimi'] = $tuote['tuotenimi'];
		$ostoskorinTuote['hinta'] = $tuote['hinta'];
		$ostoskorinTuote['maara'] = 1;

		// Kutsutaan private-metodia, joka lisää tuotteen taulukkoon tai lisää määrää, jos tuote oli
		// jo taulukossa
		$this->lisaaTuoteTaulukkoon($ostoskorinTuote,$_SESSION['kori']);
  }

  /**
   * Poistaa tuotteen ostoskorista.
   * 
   * @param int $tuote_id Poistettavan tuotteen id.
   */
  public function poista($tuote_id) { 
    // Käydään ostoskori läpi ja jos id:llä löytyy tuote, poistetaan se korista.
    for ($i = count($_SESSION['kori'])-1; $i >= 0;$i--) {
      $tuote = $_SESSION['kori'][$i];
      if ($tuote['id'] === $tuote_id) {
        array_splice($_SESSION['kori'], $i, 1);
      }
    }
  }

  /**
   * Tyhjentää ja luo uuden tyhjän ostoskorin.
   */
  public function tyhjenna() {
    $_SESSION['kori']= null;
    $_SESSION['kori'] = array();
  }

  /**
  * Lisää tuotteen taulukkoon tai päivittää määrää (+1), jos tuote on jo taulukossa.
  *
  * @param Array $tuote Tuotteen tiedot taulukossa (yksi rivi).
  * @param Array $taulukko Viittaus taulukkoon, johon tuote lisätään tai määrää päivitetään, mikäli
  *              tuote on jo taulukossa. 
	*/
  private function lisaaTuoteTaulukkoon($tuote,&$taulukko) {
    // Käydään läpi taulukon rivit.
    for ($i = 0;$i < count($taulukko);$i++ ) {
      // Jos löytyy id:llä, tuote on jo taulukossa. Päivitetään määrää.
      if ($taulukko[$i]['id'] === $tuote['id']) {
        $taulukko[$i]['maara'] = $taulukko[$i]['maara']  + 1;
        return; // Koska tuote voi olla vain kerran taulukossa, voidaan for-lause ja etsiminen
                // lopettaa, jos tuote löytyy. Return-lauseen avulla poistutaan for-lauseesta.
      }
    }
    $tuote['maara'] = 1; // Tuote ei ollut taulukossa, joten asetetaan määräksi 1.
    array_push($taulukko,$tuote);
  }


}