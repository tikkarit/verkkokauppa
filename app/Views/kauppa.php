<div class="tasaus row"> 
  <?php foreach($tuotteet as $tuote): ?>
    <div class="card col-lg-4 col-md-6 col-sm-12">
      <a href="<?= site_url('tuote/' . $tuote['id'])?>">
      <form method="post" action="<?= site_url('ostoskori/lisaa/' . $tuote['id']);?>">
        <img class="img-fluid" src="<?=base_url('img/' . $tuote['kuva']) ?>"></img>
        <h4><?= $tuote['tuotenimi'] ?></h4>
        <h4><?= $tuote['hinta'] ?> €</h4>
        <button class="bnt btn-primary"><i class="fas fa-shopping-cart">
        </i>   Lisää ostoskoriin</button>
      </form>
      </a>
    </div>
  <?php endforeach;?>
</div>
