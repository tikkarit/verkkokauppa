<div class="container">
  <div class="row">
    <div class="col-12">
      <h2 class="keskitys">Asiakastiedot / Tilaukset</h2>
    </div>
      <div class="keskitys col-lg-6 col-md-6 col-sm-12">
        <a class="no-effect" href="<?= site_url('asiakas/info'); ?>">Yhteystietosi</a>
      </div>
      <div class="keskitys col-lg-6 col-md-6 col-sm-12">
        <a class="no-effect" href="<?= site_url('asiakas/tilaukset'); ?>">Tilaukset</a>
      </div>
  </div>
</div>