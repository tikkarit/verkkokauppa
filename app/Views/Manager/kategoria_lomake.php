<h3><?= $otsikko?></h3>
<div>
  <?= \Config\Services::validation()->listErrors();?>
</div>
<form action="/manager/tallennakategoria/<?= $tuoteryhma_id?>" method="post" enctype="multipart/form-data">
  <input type="hidden" name="id" value="<?= $id?>">
  <div>
    <label>Nimi</label>
    <input name="kategoria_nimi" maxlength="50" value="<?= $kategoria_nimi?>"/>
  </div>
  <button>Tallenna</button>
</form>