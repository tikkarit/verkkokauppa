<h3><?= $otsikko?></h3>
<div>
<form action="/manager/vaihdatr/" method="post" >
<label>Valitse Tuoteryhmä</label>
<select name="tuoteryhma_id" onChange="this.form.submit()">
<?php foreach($tuoteryhmat as $tuoteryhma): ?>
  <option value="<?=$tuoteryhma['id']?>"
  <?php
  if ($tuoteryhma['id'] === $tuoteryhma_id) {
    print " selected";
  }
  ?>
  >
    <?= $tuoteryhma['trnimi']?>
  </option>
<?php endforeach;?>
</select>
</form>
</div>
<div>
<div>
  <button class="btn-success btn-lg">
    <?= anchor('manager/tallennakategoria/' . $tuoteryhma_id,'Lisää uusi')?>
  </button>
</div>
<table class="table">
<?php foreach($kategoriat as $kategoria): ?>
  <tr>
    <td><?= $kategoria['kategoria_nimi']?></td>
    <td><?= anchor('manager/tallennakategoria/' . $tuoteryhma_id . '/' . $kategoria['id'],'Muokkaa')?></td>
    <!- Kysytään varmistus, tehdäänkö poisto. -->
    <td><a href="<?= site_url('manager/poistakategoria/'. $kategoria['id'])?>" onclick="return confirm('Haluatko varmasti poistaa tuotekategorian? Myös kaikki kategorian tuotteet poistetaan.')">Poista</a></td>
  </tr>
<?php endforeach;?>
</table>