<?php namespace App\Controllers;

use App\Models\TuoteryhmaModel;
use App\Models\TuoteModel;
use App\Models\KategoriaModel;
use App\Models\OstoskoriModel;

class Tuote extends BaseController
{
  private $tuoteryhmaModel=null;
  private $kategoriaModel=null;
  private $tuoteModel=null;
  private $ostoskoriModel=null;

  function __construct()
  {
    $this->tuoteryhmaModel = new TuoteryhmaModel();
    $this->kategoriaModel = new KategoriaModel();
    $this->tuoteModel = new TuoteModel();
    $this->ostoskoriModel = new OstoskoriModel();
  }

  /**
   * Näyttää tuotteet (tuoteryhmän mukaan). 
   */
	public function index($tuote_id)
	{
    if(!isset($_SESSION['kayttajatunnus']))
		{
   			return redirect('login');
		} else if ($_SESSION['kayttajatunnus']['admin'] == 'KYLLÄ') {
        $data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
        $data['kategoriat'] = $this->kategoriaModel->haeKategoriat();  
        $data['tuote'] = $this->tuoteModel->hae($tuote_id);
        $data['ostoskori_lkm'] = $this->ostoskoriModel->lukumaara();
        echo view('templates/header_logged_admin',$data);
        echo view('tuote.php',$data);
        echo view('templates/footer');
    } else {
        $data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
        $data['kategoriat'] = $this->kategoriaModel->haeKategoriat();  
        $data['tuote'] = $this->tuoteModel->hae($tuote_id);
        $data['ostoskori_lkm'] = $this->ostoskoriModel->lukumaara();
        echo view('templates/header_logged',$data);
        echo view('tuote.php',$data);
        echo view('templates/footer');      
    }
  }
}