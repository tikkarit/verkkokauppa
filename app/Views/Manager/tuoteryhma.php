<h3><?= $otsikko?></h3>
<div>
  <button class="btn-success btn-lg">
    <?= anchor('manager/tallennatr','Lisää uusi')?>
  </button>
</div>
<table class="table">
<?php foreach($tuoteryhmat as $tuoteryhma): ?>
  <tr>
    <td><?= $tuoteryhma['trnimi']?></td>
    <td><?= anchor('manager/tallennatr/' . $tuoteryhma['id'],'Muokkaa')?></td>
    <!- Kysytään varmistus, tehdäänkö poisto. -->
    <td><a href="<?= site_url('manager/poistatr/'. $tuoteryhma['id'])?>" onclick="return confirm('Haluatko varmasti poistaa tuoteryhmän? Myös kaikki tuoteryhmän tuotteet poistetaan.')">Poista</a></td>
  </tr>
<?php endforeach;?>
</table>