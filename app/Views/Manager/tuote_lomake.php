<h3><?= $otsikko?></h3>
<div>
  <?= \Config\Services::validation()->listErrors();?>
</div>
<form action="/manager/tallenna/<?= $kategoria_id?>" method="post" enctype="multipart/form-data">
<input type="hidden" name="id" value="<?= $id?>">
  <div class="form-group">
    <label>Nimi</label>
    <input name="tuotenimi" class="form-control" maxlength="50" value="<?= $tuotenimi?>"/>
  </div>
  <div class="form-group">
    <label>Hinta</label>
    <input name="hinta" class="form-control" type="number" step="0.01" value="<?= $hinta?>"/>
  </div>
  <div class="form-group">
    <label>Kuvaus</label>
    <textarea name="kuvaus" class="form-control"><?= $kuvaus?></textarea>
  </div>
  <div class="form-group">
    <label>Kuva</label>
    <input name="kuva" class="form-control" type="file">
    <?php 
    // Jos tuotteella on kuva, tulostetaan näkyviin lomakkeelle.
    if ($kuva !== '') {
      $polku = base_url('img/thumb_' . $kuva);
      print "<img src='" .$polku  . "'/>";
    }
    ?>
  </div>
  <div class="form-group">
    <label>Varastomäärä</label>
    <input name="maara" class="form-control" type="number" step="1.00" value="<?= $maara?>"/>
  </div>
  <button>Tallenna</button>
</form>