<?php
namespace App\Controllers;

use App\Models\LoginModel;
use App\Models\TuoteryhmaModel;
use App\Models\KategoriaModel;
use App\Models\OstoskoriModel;

class Login extends BaseController
{
    private $tuoteryhmaModel=null;
	private $kategoriaModel=null;
	private $ostoskoriModel=null;

    public function __construct() {
        $this->tuoteryhmaModel = new TuoteryhmaModel();
		$this->kategoriaModel = new KategoriaModel();
		$this->ostoskoriModel = new OstoskoriModel();
        $session = \Config\Services::session();
        $session->start();
    }

    public function index()
    {
        $data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
        $data['kategoriat'] = $this->kategoriaModel->haeKategoriat();
        echo view('templates/header',$data);
        echo view('login/login', $data);
        echo view('templates/footer');
    }

    public function logout()
    {
        if(isset($_POST['logout']))
        {
            session_destroy();
            return redirect('login');
        }
    }

    public function register()
    {
        $data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
        $data['kategoriat'] = $this->kategoriaModel->haeKategoriat();
        echo view('templates/header',$data);
        echo view('login/register', $data);
        echo view('templates/footer');
    }

    public function registration() {
        $model = new LoginModel();

        if (!$this->validate([
            'kayttajatunnus' => 'required|min_length[8]|max_length[30]',
            'etunimi' => 'required|min_length[1]|max_length[100]',
            'sukunimi' => 'required|min_length[1]|max_length[100]',
            'salasana' => 'required|min_length[8]|max_length[30]',
            'salasanauudelleen' => 'required|min_length[8]|max_length[30]|matches[salasana]',
        ])){
            $data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
            $data['kategoriat'] = $this->kategoriaModel->haeKategoriat();
            $data['ostoskori_lkm'] = $this->ostoskoriModel->lukumaara();
            echo view('templates/header',$data);
            echo view('login/register',$data);
            echo view('templates/footer');
        }

        else {
            $model->save([
                'kayttajatunnus' => $this->request->getVar('kayttajatunnus'),
                'salasana' => password_hash($this->request->getVar('salasana'),PASSWORD_DEFAULT),
                'etunimi' => $this->request->getVar('etunimi'),
                'sukunimi' => $this->request->getVar('sukunimi')
            ]);
            return redirect('login');
        }
    }

    public function check() {
        $model = new LoginModel();

        if (!$this->validate([
            'kayttajatunnus' => 'required|min_length[8]|max_length[30]',
            'salasana' => 'required|min_length[8]|max_length[30]',
        ])) {
            return redirect('login');
        }
        else {
            $user = $model->check(
                $this->request->getvar('kayttajatunnus'),
                $this->request->getVar('salasana')
            );
            if ($user) {
                $_SESSION['kayttajatunnus'] = $user;
                return redirect('/');
            }
            else {
                return redirect('login');
            }
        }
    }
}