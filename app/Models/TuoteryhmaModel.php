<?php namespace App\Models;

use CodeIgniter\Model;

class TuoteryhmaModel extends Model {
    protected $table = 'tuoteryhma';
    protected $allowedFields = ['trnimi'];
  
    public function haeTuoteryhmat() {
      return $this->findAll();
    }

    public function hae($id) {
      return $this->getWhere(['id' => $id])->getRowArray();
    }

    public function haeEnsimmainenTr() {
      $this->select('id');
      $this->orderBy('id','asc');
      $this->limit(1);
      $query = $this->get();
      $tuoteryhma = $query->getRowArray();
      return $tuoteryhma['id'];
    }
  
    public function poistatr($id) {
      $this->where('id',$id);
      $this->delete();
    }
  }