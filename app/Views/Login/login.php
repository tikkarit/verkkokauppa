<form action="/login/check">
    <div class="col-12">
    <?= \Config\Services::validation()->listErrors(); ?>
    </div>
<div class="tasaus row">
<div class="kortti card col-lg-6 col-md-6 col-sm-12">
    <div class="form-group">
        <label>Käyttäjätunnus</label>
        <input
        class="form-control"
        name="kayttajatunnus"
        placeholder="Käyttäjätunnus"
        maxlength="30">
    </div>
    <div class="form-group">
        <label>Salasana</label>
        <input
        class="form-control"
        name="salasana"
        type="password"
        placeholder="Salasana"
        maxlength="30">
    </div>
</div>
<div class="kortti card col-lg-6 col-md-6 col-sm-12">
    <button class="loginbtn btn btn-primary">Kirjaudu sisään</button>
    <?= anchor('login/register','Etkö ole jo jäsen? Rekisteröidy tästä!') ?>
</div>
</div>
</form>