<div class="container">
  <div class="row">
    <div class="col-12">
    <h4 class="keskitys">Asiakkaan tiedot</h4>  
    </div>
    <div class="col-lg-6 col-md-6 col-sm-12">
    <form action="<?= site_url('asiakas/tiedot')?>" method="post">
    <?= Config\Services::validation()->listErrors();?>
    <input type="hidden" name="id" value="<?= $asiakas['id'];?>">
      <div class="form-group">
        <label>Käyttäjätunnus</label>
        <input name="kayttajatunnus" maxlength="50" class="form-control" value="<?= $asiakas['kayttajatunnus'];?>">
      </div>
      <div class="form-group">
        <label>Etunimi</label>
        <input name="etunimi" maxlength="50" class="form-control" value="<?= $asiakas['etunimi'];?>">
      </div>
      <div class="form-group">
        <label>Sukunimi</label>
        <input name="sukunimi" maxlength="100" class="form-control" value="<?= $asiakas['sukunimi'];?>">
      </div>
      <div class="form-group">
        <label>Lähiosoite</label>
        <input name="osoite" maxlength="100" class="form-control" value="<?= $asiakas['osoite'];?>">
      </div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-12">
      <div class="form-group">
        <label>Postinumero</label>
        <input name="postinro" maxlength="5" class="form-control" value="<?= $asiakas['postinro'];?>">
      </div>
      <div class="form-group">
        <label>Postitoimipaikka</label>
        <input name="postitmp" maxlength="100" class="form-control" value="<?= $asiakas['postitmp'];?>">
      </div>
      <div class="form-group">
        <label>Sähköposti</label>
        <input name="email" type="email" maxlength="255" class="form-control" value="<?= $asiakas['email'];?>">
      </div>
      <div class="form-group">
        <label>Puhelin</label>
        <input name="puhelinro" maxlength="20" class="form-control" value="<?= $asiakas['puhelinro'];?>">
      </div>
    </div>
    <div class="kortti card col-12">
      <button class="btn btn-primary">Päivitä tiedot</button>
    </div>
    </form>
</div>
</div>