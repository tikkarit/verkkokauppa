<?php namespace App\Controllers;

use App\Models\KategoriaModel;
use App\Models\TuoteryhmaModel;
use App\Models\TuoteModel;
use App\Models\AsiakasModel;
use App\Models\TilausModel;
use App\Models\OstoskoriModel;


class Manager extends BaseController
{
    private $tuoteryhmaModel=null;
    private $tuoteModel=null;
    private $kategoriaModel=null;
    private $asiakasModel=null;
    private $tilausModel=null;
    private $ostoskoriModel=null;

    function __construct()
    {
        $this->tuoteModel = new TuoteModel();
        $this->kategoriaModel = new KategoriaModel();
        $this->tuoteryhmaModel = new TuoteryhmaModel();
        $this->asiakasModel = new AsiakasModel();
        $this->tilausModel = new TilausModel();
        $this->ostoskoriModel = new OstoskoriModel();
    }

    // Managerin etusivu näyttää tuoteryhmät

    public function index()
	{
    if(!isset($_SESSION['kayttajatunnus'])) {
      return redirect('/');
    } else if ($_SESSION['kayttajatunnus']['admin'] == 'KYLLÄ') {
    $data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
    $data['otsikko'] = 'Tuoteryhmät';
    echo view('templates/header_manager.php');
    echo view('manager/tuoteryhma.php',$data);
    echo view('templates/footer_manager.php'); 
      } else {
        return redirect('/');
      }
    }

    /**
   * Tallentaa tuoteryhmän.
   * 
   * @param int $tuoteryhma_id = Tuoteryhmän id, mikäli tuoteryhmää muokataan.
   */
  public function tallennatr($tuoteryhma_id = null) {
    // Näytetään otsikko sen mukaan, ollaanko lisäämässä vai muokkaamassa.
    if ($tuoteryhma_id != null || $this->request->getPost('id')!=null) {
      $data['otsikko'] = "Muokkaa tuoteryhmää";
    }
    else {
      $data['otsikko'] = "Lisää tuoteryhmä";
    }

    // Jos post-metodi, yritetään tallentaa.
    if ($this->request->getMethod() === 'post') {
      if (!$this->validate([
        'trnimi' => 'required|max_length[50]'
      ])) {  
        // Validointi ei mene läpi, palautetaan lomake näkyviin.
        $data['id'] = $this->request->getPost('id');
        $data['trnimi'] = $this->request->getPost('trnimi');
        $this->naytatrLomake($data);
      }
      else {
        // Tallennetaan.
        $talleta['id'] = $this->request->getPost('id');
        $talleta['trnimi'] = $this->request->getPost('trnimi');
        $this->tuoteryhmaModel->save($talleta);
        return redirect('manager/index');
      }
    }
    else {
      // Näytetään lomake.
      $data['id'] = '';
      $data['trnimi'] = '';
      // Mikäli tuoteryhmä on asetettu, ollaan muokkaamassa ja haetaan tietokannasta
      // tiedot lomakkeelle.
      if ($tuoteryhma_id != null) {
        $tuoteryhma = $this->tuoteryhmaModel->hae($tuoteryhma_id);
        $data['id'] = $tuoteryhma['id'];
        $data['trnimi'] = $tuoteryhma['trnimi'];  
      }
      $this->naytatrLomake($data);
    }
  }
  /*
  * @param Array $data Lomakkeelle välitettävät muuttujat taulukossa.
  */
  private function naytatrLomake($data) {
    if(!isset($_SESSION['kayttajatunnus'])) {
        return redirect('/');
    } else if ($_SESSION['kayttajatunnus']['admin'] == 'KYLLÄ') {
        echo view('templates/header_manager.php');
        echo view('manager/tuoteryhma_lomake.php',$data);
        echo view('templates/footer_manager.php');
    } else {
        return redirect('/');
    }
  }
  

    //..............................................................................................
    // Tuotekategoriden käsittely


    // Tuotekategorioiden näyttäminen

    public function kategoriat(){
      if(!isset($_SESSION['kayttajatunnus'])) {
        return redirect('/');
      } else if ($_SESSION['kayttajatunnus']['admin'] == 'KYLLÄ') {
      $data['kategoriat'] = $this->kategoriaModel->haeKategoriat();
      $data['otsikko'] = 'Tuotekategoriat';
      echo view('templates/header_manager.php');
      echo view('manager/kategoria.php',$data);
      echo view('templates/footer_manager.php');
    } else {
      return redirect('/');
    }
    }

    public function listaakategoriat($tuoteryhma_id = null)
    {
      if ($tuoteryhma_id === null) {
        $tuoteryhma_id = $this->tuoteryhmaModel->haeEnsimmainenTr();
      }
      if(!isset($_SESSION['kayttajatunnus'])) {
        return redirect('/');
      } else if ($_SESSION['kayttajatunnus']['admin'] == 'KYLLÄ') {
      $data['tuoteryhma_id'] = $tuoteryhma_id;
      $data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
      $data['kategoriat'] = $this->kategoriaModel->haeTuoteryhmalla($tuoteryhma_id);
      $data['otsikko'] = 'Tuotekategoriat';
       echo view('templates/header_manager.php');
       echo view('manager/kategoria.php',$data);
       echo view('templates/footer_manager.php');
     } else {
       return redirect('/');
     }
     }

    public function vaihdatr() {
      $tuoteryhma_id = $this->request->getPost('tuoteryhma_id');
      $this->listaakategoriat($tuoteryhma_id);
    }
  
    /**
   * Tallentaa kategorian.
   * 
   * 
   */
  public function tallennakategoria($tuoteryhma_id,$kategoria_id = null) {
    // Näytetään otsikko sen mukaan, ollaanko lisäämässä vai muokkaamassa.
    if ($kategoria_id != null || $this->request->getPost('id')!=null) {
      $data['otsikko'] = "Muokkaa tuotekategoriaa";
    }
    else {
      $data['otsikko'] = "Lisää tuotekategoria";
    }
    
     $data['tuoteryhma'] = $tuoteryhma_id;

    // Jos post-metodi, yritetään tallentaa.
    if ($this->request->getMethod() === 'post') {
      if (!$this->validate([
        'kategoria_nimi' => 'required|max_length[50]'
      ])) {  
        // Validointi ei mene läpi, palautetaan lomake näkyviin.
        $data['tuoteryhma_id'] = $tuoteryhma_id;
        $data['id'] = $this->request->getPost('id');
        $data['kategoria_nimi'] = $this->request->getPost('kategoria_nimi');
        $this->naytakategoriaLomake($data);
      }
      else {
        // Tallennetaan.
        $talleta['tuoteryhma_id'] = $tuoteryhma_id;
        $talleta['id'] = $this->request->getPost('id');
        $talleta['kategoria_nimi'] = $this->request->getPost('kategoria_nimi');
        
        $this->kategoriaModel->save($talleta);
        return redirect()->to(site_url('/manager/listaakategoriat/' . $tuoteryhma_id));
      }
    }
    else {
      // Näytetään lomake.
      $data['tuoteryhma_id'] = $tuoteryhma_id;
      $data['id'] = '';
      $data['kategoria_nimi'] = '';
      // Mikäli kategoria on asetettu, ollaan muokkaamassa ja haetaan tietokannasta
      // tiedot lomakkeelle.
      if ($kategoria_id != null) {
        $kategoria = $this->kategoriaModel->hae($kategoria_id);
        $data['id'] = $kategoria['id'];
        $data['kategoria_nimi'] = $kategoria['kategoria_nimi'];
        $data['tuoteryhma_id'] = $tuoteryhma_id;
      }
      $this->naytakategoriaLomake($data);
    }
  }
  /**
  * Näyttää kategorian lisäys/muokkauslomakkeen.
  *
  * @param Array $data Lomakkeelle välitettävät muuttujat taulukossa.
  */
  private function naytakategoriaLomake($data) {
    if(!isset($_SESSION['kayttajatunnus'])) {
        return redirect('/');
    } else if ($_SESSION['kayttajatunnus']['admin'] == 'KYLLÄ') {
        echo view('templates/header_manager.php');
        echo view('manager/kategoria_lomake.php',$data);
        echo view('templates/footer_manager.php');
    } else {
        return redirect('/');
    }
  }
    // .............................................................................................
    // Yksittäisen tuotteen käsittely

    // Haetaan tuotteet ensimmäisestä kategoriasta
    
    public function listaatuotteet($kategoria_id = null)
	{
    if ($kategoria_id === null) {
      $kategoria_id = $this->kategoriaModel->haeEnsimmainenKategoria();
    }

    // Haetaan lomakkeella näytettävä tiedot muuttujiin.
    if(!isset($_SESSION['kayttajatunnus'])) {
      return redirect('/');
  } else if ($_SESSION['kayttajatunnus']['admin'] == 'KYLLÄ') {
    $data['kategoria_id'] = $kategoria_id;
    $data['kategoriat'] = $this->kategoriaModel->haeKategoriat();
    $data['tuotteet'] = $this->tuoteModel->haeKategorialla($kategoria_id);
    $data['otsikko'] = 'Tuotteet';
    echo view('templates/header_manager.php');
    echo view('manager/tuote.php',$data);
    echo view('templates/footer_manager.php');
  } else {
    return redirect('/');
}
  }

  // Vaihtaa post-parametrina saadun kategorian mukaiset tuotteet näkyviin.
  public function vaihdaRyhma() {
    $kategoria_id = $this->request->getPost('kategoria_id');
    $this->listaatuotteet($kategoria_id);
  }

  public function tallenna($kategoria_id,$tuote_id = null) {
    // Asetetaan otsikko sen mukaan, ollaanko lisäämässä vai muokkaamassa tuotteen tietoja.
    if ($tuote_id != null || $this->request->getPost('id')!=null) {
      $data['otsikko'] = "Muokkaa tuotetta";
    }
    else {
      $data['otsikko'] = "Lisää tuote";
    }

    // Kategorian id kulkee mukana lomakkeella aina, koska tuote pitää aina tallentaa jonkin 
    // kategorian alle.
    $data['kategoria_id'] = $kategoria_id;

    // Jos post-metodi, yritetään tallentaa.
    if ($this->request->getMethod() === 'post') {
      if (!$this->validate([
        'tuotenimi' => 'required|max_length[50]',
        'hinta' => 'required'
      ])) {  
        // Validointi ei mene läpi, palautetaan lomake näkyviin.
        $data['kategoria_id'] = $kategoria_id;
        $data['id'] = $this->request->getPost('id');
        $data['tuotenimi'] = $this->request->getPost('tuotenimi');
        $data['hinta'] = $this->request->getPost('hinta');
        $data['kuvaus'] = $this->request->getPost('kuvaus');
        $data['kuva'] = $this->request->getPost('kuva');
        $data['maara'] = $this->request->getPost('maara');
        $this->naytaLomake($data);
      }
      else {  // Tallennetaan.

        // Tallennetaan kuva ja luodaan thumbnail-samalla. Kuva ladataan, jos pystytään.
        // Tuotteen tiedot tallennetaan, vaikka kuvan lataus epäonnistuisi. Muokkaustilanteessa kuva voidaan jättää myös antamatta,
        // jolloin tuotteelle mahdollisesti jo aiemmin tallennettu kuva säilyy.
        $polku = ROOTPATH . '/public/img/';
        $tiedosto= $this->request->getFile('kuva');
        if ($tiedosto->isValid()) {
          
          $tiedosto->move($polku ,$tiedosto->getName());

          \Config\Services::image()
          ->withFile($polku . $tiedosto->getName())
          ->fit(200, 200, 'center')
          ->save($polku . 'thumb_' . $tiedosto->getName());
          $talleta['kuva'] = $tiedosto->getName();
        }

        //Tallennetaan tuote tietokantaan.
        $talleta['kategoria_id'] = $kategoria_id;
        $talleta['id'] = $this->request->getPost('id');
        $talleta['tuotenimi'] = $this->request->getPost('tuotenimi');
        $talleta['hinta'] = $this->request->getPost('hinta');
        $talleta['kuvaus'] = $this->request->getPost('kuvaus');
        $talleta['maara'] = $this->request->getPost('maara');
        $this->tuoteModel->save($talleta);
        return redirect()->to(site_url('/manager/listaatuotteet/' . $kategoria_id));
      }      
    }
    else { // Näytetään lomake, koska kyseessä on get-metodi eli lomakketta ollaan vasta tuomassa näytölle.
      $data['id'] = '';
      $data['tuotenimi'] = '';
      $data['hinta'] = 0;  
      $data['kuvaus'] = '' ;
      $data['kuva'] = '';
      $data['maara'] = 0;

      // Mikäli tuote on asetettu, ollaan muokkaamassa ja haetaan tietokannasta
      // tiedot lomakkeelle.
      if ($tuote_id != null) {
        $tuote = $this->tuoteModel->hae($tuote_id);
        $data['id'] = $tuote['id'];
        $data['tuotenimi'] = $tuote['tuotenimi'];  
        $data['hinta'] = $tuote['hinta'];  
        $data['kuvaus'] = $tuote['kuvaus'];
        $data['kuva'] = $tuote['kuva'];
        $data['maara'] = $tuote['maara'];
      }
      $this->naytaLomake($data);
    }
  }

  /**
  * Näyttää tuotteen lisäys/muokkauslomakkeen.
  *
  * @param Array $data Lomakkeelle välitettävät muuttujat taulukossa.
  */
  private function naytaLomake($data) {
    if(!isset($_SESSION['kayttajatunnus'])) {
      return redirect('/');
  } else if ($_SESSION['kayttajatunnus']['admin'] == 'KYLLÄ') {
      echo view('templates/header_manager.php');
      echo view('manager/tuote_lomake.php',$data);
      echo view('templates/footer_manager.php');
  } else {
      return redirect('/');
    }
  }
  //............................................................................
  // Asiakkaat
  public function asiakkaat() {
    if(!isset($_SESSION['kayttajatunnus'])) {
        return redirect('/');
    } else if ($_SESSION['kayttajatunnus']['admin'] == 'KYLLÄ') {
        $data['asiakkaat'] = $this->asiakasModel->haeAsiakkaat();
        $data['otsikko'] = 'Asiakkaat';
        echo view('templates/header_manager.php');
        echo view('manager/asiakas.php',$data);
        echo view('templates/footer_manager.php'); 
    } else {
        return redirect('/');
    }
  }

  ///..........................................................................
  // Tilaukset
    public function tilaukset() {
      if(!isset($_SESSION['kayttajatunnus'])) {
        return redirect('/');
      } else if ($_SESSION['kayttajatunnus']['admin'] == 'KYLLÄ') {
          $data['tilaukset'] = $this->tilausModel->haeTilaukset();
          echo view('templates/header_manager.php');
          echo view('manager/tilaus.php',$data);
          echo view('templates/footer_manager.php');
      } else {
          return redirect('/');
      }
    }
}


