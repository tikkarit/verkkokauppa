<h3><?= $otsikko?></h3>
<div>
<form action="/manager/vaihdaryhma/" method="post">
<label>Valitse tuotekategoria</label>
<select name="kategoria_id" onChange="this.form.submit()">
<?php foreach($kategoriat as $kategoria): ?>
  <option value="<?=$kategoria['id']?>"
  <?php
  if ($kategoria['id'] === $kategoria_id) {
    print " selected";
  }
  ?>
  >
    <?= $kategoria['kategoria_nimi']?>
  </option>
<?php endforeach;?>
</select>
</form>
</div>
<div>
  <button class="btn-success btn-lg">
    <?= anchor('manager/tallenna/' . $kategoria_id,'Lisää uusi')?>
  </button>
</div>
<table class="table">
<?php foreach($tuotteet as $tuote): ?>
  <tr>
    <td><?= $tuote['tuotenimi']?></td>
    <td><?= $tuote['hinta']?> €</td>
    <td><?= $tuote['maara']?></td>
    <td><?= anchor('manager/tallenna/' . $kategoria_id . '/' . $tuote['id'],'Muokkaa')?></td>
  </tr>
<?php endforeach;?>
</table>