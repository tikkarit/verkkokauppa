<?php namespace App\Controllers;

use App\Models\TuoteryhmaModel;
use App\Models\TuoteModel;
use App\Models\OstoskoriModel;
use App\Models\KategoriaModel;

class Kauppa extends BaseController
{
	private $tuoteryhmaModel=null;
	private $kategoriaModel=null;
	private $tuoteModel=null;
	private $ostoskoriModel = null;

	function __construct()
	{
		$this->tuoteryhmaModel = new TuoteryhmaModel();
		$this->kategoriaModel = new KategoriaModel();
		$this->tuoteModel = new TuoteModel();
		$this->ostoskoriModel = new OstoskoriModel();
	}
	public function index($kategoria_id)
	{
		if(!isset($_SESSION['kayttajatunnus']))
		{
   			return redirect('login');
		} else if ($_SESSION['kayttajatunnus']['admin'] == 'KYLLÄ') {
			$data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
			$data['kategoriat'] = $this->kategoriaModel->haeKategoriat();
			$data['tuotteet'] = $this->tuoteModel->haeKategorialla($kategoria_id);
			$data['ostoskori_lkm'] = $this->ostoskoriModel->lukumaara();
			echo view('templates/header_logged_admin',$data);
			echo view('kauppa',$data);
			echo view('templates/footer');
		} else {
			$data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
			$data['kategoriat'] = $this->kategoriaModel->haeKategoriat();
			$data['tuotteet'] = $this->tuoteModel->haeKategorialla($kategoria_id);
			$data['ostoskori_lkm'] = $this->ostoskoriModel->lukumaara();
			echo view('templates/header_logged',$data);
			echo view('kauppa',$data);
			echo view('templates/footer');
		}
	}

	public function index2($tuoteryhma_id)
	{
		if(!isset($_SESSION['kayttajatunnus']))
		{
   			return redirect('login');
		} else if ($_SESSION['kayttajatunnus']['admin'] == 'KYLLÄ') {
			$data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
			$data['kategoriat'] = $this->kategoriaModel->haeKategoriat();
			$data['tuotteet'] = $this->tuoteModel->haeTuoteryhmalla($tuoteryhma_id);
			$data['ostoskori_lkm'] = $this->ostoskoriModel->lukumaara();
			echo view('templates/header_logged_admin',$data);
			echo view('kauppa',$data);
			echo view('templates/footer');
		} else {
			$data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
			$data['kategoriat'] = $this->kategoriaModel->haeKategoriat();
			$data['tuotteet'] = $this->tuoteModel->haeTuoteryhmalla($tuoteryhma_id);
			$data['ostoskori_lkm'] = $this->ostoskoriModel->lukumaara();
			echo view('templates/header_logged',$data);
			echo view('kauppa',$data);
			echo view('templates/footer');			
		}
	}

	public function etsi() {
		if ($_SESSION['kayttajatunnus']['admin'] == 'KYLLÄ') {
			$nimi = $this->request->getPost('etsi');
			$data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
			$data['kategoriat'] = $this->kategoriaModel->haeKategoriat();
			$data['tuotteet'] = $this->tuoteModel->haeNimella($nimi);
			$data['ostoskori_lkm'] = $this->ostoskoriModel->lukumaara();
			echo view('templates/header_logged_admin',$data);
			echo view('kauppa',$data);
			echo view('templates/footer');
		} else {
			$nimi = $this->request->getPost('etsi');
			$data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
			$data['kategoriat'] = $this->kategoriaModel->haeKategoriat();
			$data['tuotteet'] = $this->tuoteModel->haeNimella($nimi);
			$data['ostoskori_lkm'] = $this->ostoskoriModel->lukumaara();
			echo view('templates/header_logged',$data);
			echo view('kauppa',$data);
			echo view('templates/footer');			
		}
	}
}