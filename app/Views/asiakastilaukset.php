<?php
use App\Libraries\Util;
?>
<div class="col-12">
<table class="table">
  <tr>
    <th>Tilausnumero</th>
    <th>Päivämäärä</th>
    <th>Tuote</th>
    <th>Määrä</th>
    <!--<th></th>-->
    <th></th>
  </tr>
<?php
$tilaus_id = 0;
?>
<?php foreach($tilaukset as $tilaus): ?>
<tr>
  <?php if ($tilaus_id != $tilaus['tilausid']) {?>
  <td><?=$tilaus['tilausid']?></td> 
  <td><?=Util::sqlDateToFi($tilaus['tilauspvm'])?></td> 
  <?php } else { ?>
    <td></td><td></td>
  <?php } ?>  

  <td><?=$tilaus['tuotenimi']?></td>
  <td><?=$tilaus['kpl']?></td>

  <?php if ($tilaus_id != $tilaus['tilausid']) {?>
    <!-- <td><a href="#">Toimita</a></td> -->
    
  <?php } else {?>
    <!-- <td></td> -->
    <td></td>
  <?php } ?>  
  <?php
  $tilaus_id = $tilaus['tilausid'];
  ?>
</tr>
<?php endforeach;?>
</table>
</div>