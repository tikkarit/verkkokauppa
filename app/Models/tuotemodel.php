<?php namespace App\Models;

use CodeIgniter\Model;

class TuoteModel extends Model {
    protected $table = 'tuote';

    protected $allowedFields = ['id','tuotenimi','hinta','kuvaus','kuva','kategoria_id','maara'];

    public function haeKategorialla($kategoria_id) {
        return $this->getWhere(['kategoria_id' => $kategoria_id])->getResultArray();
    }

    public function haeTuoteryhmalla($tuoteryhma_id){
      $this->table('tuote');
      $this->select('tuote.id,tuote.tuotenimi,tuote.hinta,tuote.kuva,kategoria.tuoteryhma_id');
      $this->join('kategoria','tuote.kategoria_id=kategoria.id');
      return $this->getWhere(['tuoteryhma_id' => $tuoteryhma_id])->getResultArray();

    }

    public function haeTuote($id) {
        $this->where('id',$id);
        $query = $this->get();
        $tuote = $query->getRowArray();
        // Voidaan käyttää debuggauksessa, kun halutaan tietää, mikä
        // kysely suoritettiin.
        //echo $this->getLastQuery(); 
        return $tuote;
      }

      public function haeTuotteet($idt) {
        $palautus = array();
        foreach ($idt as $id) {
          $this->table('tuote');
          $this->select('id,tuotenimi,hinta');
          $this->where('id',$id);
          $query = $this->get();
          $tuote = $query->getRowArray();
          array_push($palautus,$tuote);
            
          $this->resetQuery();
        }
       
        return $palautus;
      }
      public function hae($id) {
        $this->where('id',$id);
        $query = $this->get();
        $tuote = $query->getRowArray();
        // Voidaan käyttää debuggauksessa, kun halutaan tietää, mikä
        // kysely suoritettiin.
        //echo $this->getLastQuery(); 
        return $tuote;
      }

      public function haeNimella($nimi) {
        $this->like('tuotenimi',$nimi);
        $query = $this->get();
        return $query->getResultArray();
      }

      public function poistaKategorialla($kategoria_id) {
        $this->where('kategoria_id',$kategoria_id);
        $this->delete();
  }


    }      
