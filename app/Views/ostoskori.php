<div class="container kori1">
  <div class="row">  
    <div class="col-lg-7 col-md-7 col-sm-12">
    <h4>Ostoskori</h4>
    <?php
    $summa = 0;
    ?>
    <table class="table">
    <?php foreach ($tuotteet as $tuote): ?>
      <tr>
        <td>
          <?= $tuote['tuotenimi']?>
        </td>  
        <td>
          <?= $tuote['hinta'] . ' €'?>
        </td>
        <td>
        <?= $tuote['maara']?>
        </td>
        <td>
          <a class="ostoskori_poista"href="<?= site_url('ostoskori/poista/' . $tuote['id'])?>">
            <i class="fas fa-minus-circle"></i>
          </a>
        </td>
      </tr>
      <?php
      $summa += $tuote['hinta'] * $tuote['maara'];
      ?>
    <?php endforeach;?>
    <tr>
      <td>Yhteensä</td>
      <td><?= sprintf("%.2f €", $summa);?></td>
      <td></td>
      <td>   
        <a id="tyhjenna" href="<?= site_url('ostoskori/tyhjenna');?>">
          <i class="fas fa-trash"></i>
        </a>
      </td>
    </tr>
    </table>
    </div>
    <div class="col-lg-5 col-md-5 col-sm-12">
    <h4>Tilaajan tiedot</h4>
    <form action="<?= site_url('ostoskori/tilaa')?>" method="post">
      <div>
        <input type="hidden" name="id" value="<?= $asiakas['id'];?>">
      </div>
      <div class="form-goup">
        <p>Huom! Muistathan tarkistaa tilaustietosi. Tietoja voit päivittää Käyttäjä sivullasi.</p>
      </div>
      <div class="form-group">
        <label>Etunimi</label>
        <p class="kori2" name="etunimi" maxlength="50" class="form-control" ><?= $asiakas['etunimi'];?></p>
      </div>
      <div class="form-group">
        <label>Sukunimi</label>
        <p class="kori2" name="sukunimi" maxlength="100" class="form-control"><?= $asiakas['sukunimi'];?></p>
      </div>
      <div class="form-group">
        <label>Lähiosoite</label>
        <p class="kori2" name="osoite" maxlength="100" class="form-control"><?= $asiakas['osoite'];?></p>
      </div>
      <div class="form-group">
        <label>Postinumero</label>
        <p class="kori2" name="postinro" maxlength="5" class="form-control"><?= $asiakas['postinro'];?></p>
      </div>
      <div class="form-group">
        <label>Postitoimipaikka</label>
        <p class="kori2" name="postitmp" maxlength="100" class="form-control"><?= $asiakas['postitmp'];?></p>
      </div>
      <div class="form-group">
        <label>Sähköposti</label>
        <p class="kori2" name="email" type="email" maxlength="255" class="form-control"><?= $asiakas['email'];?></p>
      </div>
      <div class="form-group">
        <label>Puhelin</label>
        <p class="kori2" name="puhelinro" maxlength="20" class="form-control"><?= $asiakas['puhelinro'];?></p>
      </div>
      <div class="kortti card col-12">
        <button class="btn btn-primary tilaa">Tilaa</button>
      </div>
    </form>
  </div>
  </div>
  </div>