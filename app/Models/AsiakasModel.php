<?php  namespace App\Models;

use CodeIgniter\Model;

/**
 * Sisältää asiakas-taulun käsittelyyn liittyviä metodeja.
 */
class AsiakasModel extends Model {
  protected $table = 'asiakas'; // Malli käsittelee asiakas-taulua tietokannassa.

  // Luettelo niistä kentistä, joita päivitetään, kun ajetaan tallennus (esim. save) tietokantaan.
  protected $allowedFields = ['id','kayttajatunnus','admin','etunimi','sukunimi','osoite','postinro','postitmp','email','puhelinro'];

  public function haeAsiakkaat() {
    return $this->findAll();
  }

  public function haeAsiakas($id) {
    $this->where('id',$id);
        $query = $this->get();
        $asiakas = $query->getRowArray(); 
        return $asiakas;
  }
}

