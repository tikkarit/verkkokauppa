<form method="post" action="<?= site_url('ostoskori/lisaa/' . $tuote['id']);?>">
  <div class="tasaus row">
    <div class="card col-lg-6 col-md-6 col-sm-12">
      <img class="img-fluid" src="<?= base_url('img/' . $tuote['kuva'])?>" alt="">
      </div>
      <div class="card col-lg-6 col-md-6 col-sm-12">
      <h3><?= $tuote['tuotenimi'];?></h3>
      <p><?= $tuote['kuvaus'];?></p>
      <div>
        <div>
        <p class="hinta"><?= $tuote['hinta'];?> €</p>
        </div>
        <div>
        <p>Varastomäärä: <?= $tuote['maara'];?></p>
        </div>
      </div>
      <button class="bnt btn-primary osta">Lisää ostoskoriin</button>
      </div>
    </div>
  </div>
</form>