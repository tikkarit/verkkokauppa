<?php  namespace App\Models;

use CodeIgniter\Model;

/**
 * Sisältää asiakas-taulun käsittelyyn liittyviä metodeja.
 */
class TilausModel extends Model {
  protected $table = 'tilaus'; // Malli käsittelee tilaus-taulua tietokannassa.

  // Luettelo niistä kentistä, joita päivitetään, kun ajetaan tallennus (esim. save) tietokantaan.
  protected $allowedFields = ['id','tilauspvm','asiakas_id','tapa','tila']; 


  /**
   * Hakee tilaukset tietokannasta.
   */
  public function haeTilaukset() {
    $this->select('asiakas.etunimi as etunimi,
    asiakas.sukunimi as sukunimi,
    tilaus.id as tilausid,
    tilaus.tilauspvm,
    tilausrivi.kpl,
    tuote.tuotenimi');
    $this->join('asiakas','asiakas.id = tilaus.asiakas_id');
    $this->join('tilausrivi','tilausrivi.tilaus_id = tilaus.id');
    $this->join('tuote','tuote.id = tilausrivi.tuote_id');
    $kysely = $this->get();
    return $kysely->getResultArray();
  }

  public function haeAsiakkaantilaukset($id) {
    $this->select('asiakas.etunimi as etunimi,
    asiakas.sukunimi as sukunimi,
    tilaus.id as tilausid,
    tilaus.tilauspvm,
    tilausrivi.kpl,
    tuote.tuotenimi');
    $this->join('asiakas','asiakas.id = tilaus.asiakas_id');
    $this->join('tilausrivi','tilausrivi.tilaus_id = tilaus.id');
    $this->join('tuote','tuote.id = tilausrivi.tuote_id');
    $kysely = $this->getWhere(['asiakas.id'=>$id]);
    return $kysely->getResultArray();
  }

  /**
  * Poistaa tilauksen.
  * 
  * @param int $id Poistettavan tilauksen id.
  */
  public function poista($id) {
    $this->where('id',$id);
    $this->delete();
  }
}