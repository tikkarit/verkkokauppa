<form action="/login/registration">
    <div class="col-12">
    <?= \Config\Services::validation()->listErrors(); ?>
    </div>
    <div class="tasaus row">
    <div class="kortti card col-lg-6 col-md-6 col-sm-12">
    <div class="form-group">
        <label>Käyttäjätunnus</label>
        <input
        class="form-control"
        name="kayttajatunnus"
        placeholder="Syötä käyttäjätunnus"
        maxlength="30">
    </div>
    <div class="form-group">
        <label>Etunimi</label>
        <input
        class="form-control"
        name="etunimi"
        placeholder="Syötä etunimi"
        maxlength="30">
    </div>
    <div class="form-group">
        <label>Sukunimi</label>
        <input
        class="form-control"
        name="sukunimi"
        placeholder="Syötä sukunimi"
        maxlength="30">
    </div>
    <div class="form-group">
        <label>Salasana</label>
        <input
        class="form-control"
        name="salasana"
        type="password"
        placeholder="Syötä salasana"
        maxlength="30">
    </div>
    <div class="form-group">
        <label>Salasana uudelleen</label>
        <input
        class="form-control"
        name="salasanauudelleen"
        type="password"
        placeholder="Syötä salasana uudelleen"
        maxlength="30">
    </div>
</div>
    <div class="kortti card col-lg-6 col-md-6 col-sm-12">
    <button class="registerbtn btn btn-primary">Luo tili</button>
</div>
</div>
</form>