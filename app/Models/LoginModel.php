<?php namespace App\Models;

use CodeIgniter\Model;

class LoginModel extends Model {
    protected $table = 'asiakas';

    protected $allowedFields = ['id','kayttajatunnus','salasana','admin','etunimi','sukunimi','osoite','postinro','postitmp','puhelinro'];

    public function check($username, $salasana) {
        $this->where('kayttajatunnus', $username);
        $query = $this->get();
        $row = $query->getRowArray();
        if ($row) {
            if (password_verify($salasana, $row['salasana'])) {
                return $row;
            }
        }
        return null;
    }
}