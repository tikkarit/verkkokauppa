<div id="bgimg-1" class="parallax">
            <div class="caption">
                <span class="border">Pieni olutpuoti</span>
            </div>
        </div>
        <div class="text">
            <h3>Mikä Pieni olutpuoti on?</h3>
            <p>Olemme Suomen ensimmäinen, paras ja ainoa pienpanimo-oluiden verkkokauppa. Myymme yhteistyössä
                lukuisten arvostettujen suomalaisten käsityöläispanimoiden kanssa maamme maukkaimpia 
                oluita yksityishenkilöille kotiinkuljetettuna, viilennystä vaille nautittavina.
            </p>            
        </div>

        <div id="bgimg-3" class="parallax">
        </div>

        <div class="text">
            <h3>Valikoima</h3>
            <p>Valikoimastamme löydätte tuotteita kaikista suomalaisista panimoista, jotka 
                täyttävät käsityöläispanimon säädetyn määritelmän. Tuotteitamme löytyy vain 
                harvojen kauppojen hyllyiltä, eikä yhdestäkään kaupasta varmasti kaikkia. 
            </p>
            <p>Lisäksi myymme parhaita kotimaisia naposteltavia maailman parhaiden oluiden kyytipojaksi. 
                Tyylitietoisille oluen ystäville tarjolla on myös nautintoa edistäviä välineitä, kuten tuoppeja ja laseja 
                sekä muuta krääsää.
            </p>
        </div>

        <div id="bgimg-2" class="parallax">
        </div>

        <div class="text">
            <h3>Toimitus</h3>
            <p>Tuotteet toimitetaan kotiisi tai vaihtoehtoisesti valitsemaasi toimituspisteeseen. 
                Pakkaamme tuotteet sopivan kokoisiin ja turvallisesti pehmustettuihin laatikoihin, 
                jotta tuotteet saapuvat varmasti ehjänä perille.
            </p>
            <p>Toimitusmaksu riippuu toimitustavasta ja -nopeudesta. Kaikki yli 150 € tilaukset 
                toimitamme kotiisi ilmaiseksi.
            </p>            
        </div>

        <div id="bgimg-4" class="parallax">
        </div>

        <div class="text">
            <h3>Porukka</h3>
            <p>Olemme neljä tietojenkäsittelyn opiskelijaa, jotka päättivät tehdä Suomen edistyksellisimmän 
                verkkokaupan. Siinä onnistuimme.
            </p>
        </div>

        <div id="bgimg-6" class="parallax">
        <div class="caption">
                <span class="border">Oma oluesi myyntiin?</span>
            </div>
        </div>

        <div class="text">
            <h3>Mestaripanijaksi?</h3>
            <p>Oletko kova panemaan? Mikäli kotipanimosi tuotanto on jo sillä tasolla, että 
            oluttasi riittäisi jo muillekin, sinulla on mahdollisuus saada rakkautesi tuotteet 
            meille myyntiin.
            </p>
            <p>Otamme jatkuvasti valikoimaamme uusia kotipanimoiden tuotteita. Verkkokauppamme 
                kautta kaikilla on mahdollisuus saada arvostusta omalle reseptilleen ja rohkaistua 
                ammattimaisen panimon perustamisessa!
            </p>
        </div>

        <div id="bgimg-5" class="parallax">
            <div class="caption">
                <span class="border">Sukella olueen! Tai ainakin sen maailmaan.</span>
            </div>
        </div>

        

