<?php namespace App\Models;

use CodeIgniter\Model;

class KategoriaModel extends Model {
    protected $table = 'kategoria';
    protected $allowedFields = ['id','kategoria_nimi','tuoteryhma_id'];
    
    public function haeKategoriat() {
      return $this->findAll();
    }

    public function hae($id) {
      return $this->getWhere(['id' => $id])->getRowArray();
    }

    public function haeEnsimmainenKategoria() {
      $this->select('id');
      $this->orderBy('id','asc');
      $this->limit(1);
      $query = $this->get();
      $kategoria = $query->getRowArray();
      return $kategoria['id'];
    }

    public function haeTuoteryhmalla($tuoteryhma_id) {
      return $this->getWhere(['tuoteryhma_id' => $tuoteryhma_id])->getResultArray();
  }
  
    public function poistakategoria($id) {
      $this->where('id',$id);
      $this->delete();
    }

    public function poistaTuoteryhmalla($tuoteryhma_id) {
      $this->where('tuoteryhma_id',$tuoteryhma_id);
      $this->delete();
}
  }