<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap, Font Awesome, CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
    <link rel="stylesheet" href="<?= base_url('/css/style.css');?>">
    <link rel="stylesheet" href="<?= base_url('/css/parallax.css');?>">
    <title>Pieni olutpuoti</title>

  </head>
  <body>
    <main class="row">
      <header class="row">
        <figure>
          <img class="img-fluid" src=<?= base_url('img/logo.png');?> alt="pieni olutpuoti"/>
        </figure>
          <nav class="navbar navbar-expand-md navbar-oma">
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span><i class="fas fa-beer"></i></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navblock navbar-nav">
              <li class="nav-item">
                <a class="nav-link" href="<?=site_url('')?>">Etusivu<span class="sr-only">(current)</span></a>
              </li>
              <?php foreach($tuoteryhmat as $tuoteryhma): ?>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="<?=site_url('kauppa/index2/' . $tuoteryhma['id']) ?>"><?=$tuoteryhma['trnimi']?></a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <?php foreach($kategoriat as $kategoria): {
                  if ($kategoria['tuoteryhma_id'] !== $tuoteryhma['id']) {continue;}
                  } ?>
                    <a class="tuote dropdown-item" href="<?=site_url('kauppa/index/' . $kategoria['id']) ?>"><?=$kategoria['kategoria_nimi']?></a>
                <?php endforeach;?>
              <?php endforeach;?>
                </div>
              </li>
              <li class="nav-item">
            <a id="kori" class="nav-link" href="<?= site_url('ostoskori/index'); ?>">
              <i class="fas fa-shopping-cart">
              </i>
              <span id="lkm"><?= $ostoskori_lkm?></span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= site_url('asiakas'); ?>"><i id="user" class="fas fa-user"></i> Käyttäjä
            </a>
          </li>
          <li class="nav-item">
          <form class="nav-link" method="post" action="<?= site_url('login/logout');?>">
            <input class="logout" type="submit" name="logout" value="Kirjaudu ulos">
          </form>
          </li>
            </ul>
          </div>
        </nav>
        <nav class="navbar navbar-expand-md navbar-oma col-12">
        <div id="etsinavi" >
          <form class="form-inline my-2" method="post" action="<?= site_url('kauppa/etsi/')?>">
            <input id="input" class="form-control mr-sm-2" type="search" aria-label="Search" name="etsi" placeholder="Etsi tuotteita">
            <button class="btn btn-outline-secondary my-2 my-sm-0" type="submit">
              <i class="fas fa-search"></i>
            </button>
          </form>
          </div>
        </nav>
      </header>
    </main>