<h3><?= $otsikko?></h3>
<table class="table">
  <tr>
    <th>Asiakasnro</th>
    <th>Käyttäjätunnus</th>
    <th>Etunimi</th>
    <th>Sukunimi</th>
    <th>Osoite</th>
    <th>Postinnumero</th>
    <th>Postitoimipaikka</th>
    <th>Email</th>
    <th>Puh.</th>
    <th>Admin</th>
  </tr>
  <?php foreach($asiakkaat as $asiakas): ?>
  <tr>
    <td><?= $asiakas['id']?></td>
    <td><?= $asiakas['kayttajatunnus']?></td>
    <td><?= $asiakas['etunimi']?></td>
    <td><?= $asiakas['sukunimi']?></td>
    <td><?= $asiakas['osoite']?></td>
    <td><?= $asiakas['postinro']?></td>
    <td><?= $asiakas['postitmp']?></td>
    <td><?= $asiakas['email']?></td>
    <td><?= $asiakas['puhelinro']?></td>
    <td><?= $asiakas['admin']?></td>
  </tr>
<?php endforeach;?>
</table>