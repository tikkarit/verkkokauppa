<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap, Font Awesome, CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
    <link rel="stylesheet" href="<?= base_url('/css/mstyle.css');?>">
    <title>PoP Manager</title>

  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light ">
      <a class="navbar-brand" href="manager/index">PoP Manager</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">    
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="<?=site_url('manager/index/')?>">Tuoteryhmät</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?=site_url('manager/listaakategoriat')?>">Tuotekategoriat</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?=site_url('manager/listaatuotteet')?>">Tuotteet</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?=site_url('manager/tilaukset')?>">Tilaukset</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?=site_url('manager/asiakkaat')?>">Asiakkaat</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?=site_url('/')?>">Etusivulle</a>
          </li>
        </ul>  
      </div>
    </nav>  
    <div class="container">