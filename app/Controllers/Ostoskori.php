<?php  namespace App\Controllers;

use App\Models\TuoteryhmaModel;
use App\Models\OstoskoriModel;
use App\Models\KategoriaModel;
use App\Models\AsiakasModel;

/**
 * Ostoskori-toiminnallisuuden käsittelijä.
 */
class Ostoskori extends BaseController
{
  private $tuoteryhmaModel=null;
  private $kategoriaModel=null;
  private $ostoskoriModel=null;
  private $asiakasModel=null;

  public function __construct() {
    $this->tuoteryhmaModel = new TuoteryhmaModel();
    $this->kategoriaModel = new KategoriaModel();
	$this->ostoskoriModel = new OstoskoriModel();
	$this->asiakasModel = new AsiakasModel();
	}

	/**
	 * Näyttää ostoskorin sisällön.
	 */
  public function index()
	{
		if(!isset($_SESSION['kayttajatunnus']))
		{
   			return redirect('login');
		} else if ($_SESSION['kayttajatunnus']['admin'] == 'KYLLÄ') {
			$data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
        	$data['kategoriat'] = $this->kategoriaModel->haeKategoriat();
    		$data['tuotteet'] = $this->ostoskoriModel->ostoskori();
			$data['ostoskori_lkm'] = $this->ostoskoriModel->lukumaara();
			$data['asiakas'] = $_SESSION['kayttajatunnus'];
			echo view('templates/header_logged_admin',$data );
			echo view('ostoskori',$data);
			echo view('templates/footer');
		} else {
			$data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
        	$data['kategoriat'] = $this->kategoriaModel->haeKategoriat();
    		$data['tuotteet'] = $this->ostoskoriModel->ostoskori();
			$data['ostoskori_lkm'] = $this->ostoskoriModel->lukumaara();
			$data['asiakas'] = $_SESSION['kayttajatunnus'];
			echo view('templates/header_logged',$data );
			echo view('ostoskori',$data);
			echo view('templates/footer');
		}
	}

	/**
	 * Tallentaa tilauksen.
	 */
	public function tilaa()
	{
		$data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
    	$asiakas = [
	  'id' => $this->request->getPost('id')
    			   ];

		$this->ostoskoriModel->tilaa($asiakas);

		if ($_SESSION['kayttajatunnus']['admin'] == 'KYLLÄ') {
		
			$data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
			$data['kategoriat'] = $this->kategoriaModel->haeKategoriat();
			$data['ostoskori_lkm'] = $this->ostoskoriModel->lukumaara();
			echo view('templates/header_logged_admin',$data);
			echo view('kiitos',$data);
			echo view('templates/footer');
			}
			else {
			$data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
			$data['kategoriat'] = $this->kategoriaModel->haeKategoriat();
			$data['ostoskori_lkm'] = $this->ostoskoriModel->lukumaara();
			echo view('templates/header_logged',$data);
			echo view('kiitos',$data);
			echo view('templates/footer');
			}
	}

	/**
	 * Lisää tuotteen ostoskoriin.
	 */
  public function lisaa($tuote_id) { 
		$this->ostoskoriModel->lisaa($tuote_id);
		return redirect()->back();
  }

	/**
	 * Tyhjentää ostoskorin.
	 */
  public function tyhjenna() {
		$this->ostoskoriModel->tyhjenna();
    return redirect()->to(site_url('ostoskori/index'));		
	}
	
	/**
	 * Poistaa valitun tuotteen ostoskorista.
	 */
	public function poista($tuote_id) {
		$this->ostoskoriModel->poista($tuote_id);
		return redirect()->to(site_url('ostoskori/index'));	
	}

}